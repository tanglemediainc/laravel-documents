<?php

namespace TangleMedia\Laravel\Documents\Filters;

class ParentFolderIdentifierInFilter
{
    public function filter($builder, $value)
    {
        $ids = explode('|', $value);
        return $builder->whereIn('parent_folder_id', $ids);
    }
}
