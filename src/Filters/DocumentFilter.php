<?php

namespace TangleMedia\Laravel\Documents\Filters;

class DocumentFilter extends AbstractFilter
{
    protected $filters = [
        'id' => IdentifierInFilter::class,
        'name' => NameFilter::class,
        'filename' => FilenameFilter::class,
        'document_folder_id' => DocumentFolderInFilter::class,
    ];
}
