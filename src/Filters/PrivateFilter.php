<?php

namespace TangleMedia\Laravel\Documents\Filters;

class PrivateFilter
{
    public function filter($builder, $value)
    {
        return $builder->where('private', '=', $value);
    }
}
