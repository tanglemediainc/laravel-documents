<?php

namespace TangleMedia\Laravel\Documents\Filters;

class NameFilter
{
    public function filter($builder, $value)
    {
        return $builder->where('name', 'like', "%$value%");
    }
}
