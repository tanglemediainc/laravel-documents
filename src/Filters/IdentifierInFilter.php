<?php

namespace TangleMedia\Laravel\Documents\Filters;

class IdentifierInFilter
{
    public function filter($builder, $value)
    {
        $ids = explode('|', $value);
        return $builder->whereIn('id', $ids);
    }
}
