<?php

namespace TangleMedia\Laravel\Documents\Filters;

class FilenameFilter
{
    public function filter($builder, $value)
    {
        return $builder->where('filename', 'like', "%$value%");
    }
}
