<?php

namespace TangleMedia\Laravel\Documents\Filters;

class DocumentFolderFilter extends AbstractFilter
{
    protected $filters = [
        'id' => IdentifierInFilter::class,
        'name' => NameFilter::class,
        'parent_folder_id' => ParentFolderIdentifierInFilter::class,
        'private' => PrivateFilter::class,
    ];
}
