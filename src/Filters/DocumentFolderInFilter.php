<?php

namespace TangleMedia\Laravel\Documents\Filters;

class DocumentFolderInFilter
{
    public function filter($builder, $value)
    {
        $ids = explode('|', $value);
        return $builder->whereIn('document_folder_id', $ids);
    }
}
