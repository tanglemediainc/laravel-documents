<?php

namespace TangleMedia\Laravel\Documents\Interfaces\Repositories;

use TangleMedia\Laravel\Documents\Helpers\DocumentUploadFile;

interface DocumentRepositoryInterface
{
    public function paginate();

    public function findOne(string $document_id);

    public function create(DocumentUploadFile $file);

    public function update(DocumentUploadFile $file, string $document_id);

    public function delete(string $document_id);
}
