<?php

namespace TangleMedia\Laravel\Documents\Interfaces\Repositories;

interface DocumentFolderRepositoryInterface
{
    public function paginate();

    public function findOne(string $document_folder_id);

    public function create(array $data);

    public function update(array $data, string $document_folder_id);

    public function delete(string $document_folder_id);
}
