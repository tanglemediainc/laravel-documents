<?php

namespace TangleMedia\Laravel\Documents\Interfaces\Services;

use TangleMedia\Laravel\Documents\Helpers\DocumentUploadFile;
use TangleMedia\Laravel\Documents\Http\Requests\DocumentAttachRequest;
use TangleMedia\Laravel\Documents\Http\Requests\DocumentStoreRequest;
use TangleMedia\Laravel\Documents\Http\Requests\DocumentUpdateRequest;

interface DocumentServiceInterface
{
    public function getPaginated();

    public function getOne(string $document_id);

    public function store(DocumentStoreRequest $request, DocumentUploadFile $file);

    public function update(DocumentUpdateRequest $request, DocumentUploadFile $file, string $document_id);

    public function delete(string $document_id);

}
