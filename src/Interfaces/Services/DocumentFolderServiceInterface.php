<?php

namespace TangleMedia\Laravel\Documents\Interfaces\Services;

use TangleMedia\Laravel\Documents\Http\Requests\DocumentFolderStoreRequest;
use TangleMedia\Laravel\Documents\Http\Requests\DocumentFolderUpdateRequest;

interface DocumentFolderServiceInterface
{
    public function getPaginated();

    public function getOne(string $document_folder_id);

    public function store(array $data);

    public function update(array $data, string $document_folder_id);

    public function delete(string $document_folder_id);
}
