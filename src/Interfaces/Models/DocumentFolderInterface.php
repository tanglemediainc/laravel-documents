<?php

namespace TangleMedia\Laravel\Documents\Interfaces\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

interface DocumentFolderInterface
{
    /**
     * @return HasMany
     */
    public function documents(): HasMany;

    /**
     * @return BelongsTo
     */
    public function parentFolder(): BelongsTo;

    /**
     * @return HasMany
     */
    public function childrenFolders(): HasMany;
}
