<?php

namespace TangleMedia\Laravel\Documents\Interfaces\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

interface DocumentInterface
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function documentFolder(): BelongsTo;
}
