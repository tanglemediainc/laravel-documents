<?php

namespace TangleMedia\Laravel\Documents\Exceptions;

use Exception;

class EmptyFileException extends Exception {}
