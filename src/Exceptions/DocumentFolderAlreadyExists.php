<?php

namespace TangleMedia\Laravel\Documents\Exceptions;

use InvalidArgumentException;

class DocumentFolderAlreadyExists extends InvalidArgumentException
{
    public static function create(string $folderName)
    {
        return new static("A document folder `{$folderName}` already exists.");
    }
}
