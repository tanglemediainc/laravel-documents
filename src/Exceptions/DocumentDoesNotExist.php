<?php

namespace TangleMedia\Laravel\Documents\Exceptions;

use InvalidArgumentException;

class DocumentDoesNotExist extends InvalidArgumentException
{
    public static function create(string $id)
    {
        return new static("There is no document with id `{$id}`.");
    }
}
