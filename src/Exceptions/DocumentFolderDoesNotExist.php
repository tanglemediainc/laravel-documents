<?php

namespace TangleMedia\Laravel\Documents\Exceptions;

use InvalidArgumentException;

class DocumentFolderDoesNotExist extends InvalidArgumentException
{
    public static function create(string $folderName)
    {
        return new static("There is no document folder named `{$folderName}`.");
    }

    public static function withId(int $folderId)
    {
        return new static("There is no document folder with id `{$folderId}`.");
    }
}
