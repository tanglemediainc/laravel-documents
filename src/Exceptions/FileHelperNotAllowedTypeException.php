<?php

namespace TangleMedia\Laravel\Documents\Exceptions;

use Exception;

class FileHelperNotAllowedTypeException extends Exception {}
