<?php

namespace TangleMedia\Laravel\Documents\Services;

use TangleMedia\Laravel\Documents\Http\Requests\DocumentFolderStoreRequest;
use TangleMedia\Laravel\Documents\Http\Requests\DocumentFolderUpdateRequest;
use TangleMedia\Laravel\Documents\Interfaces\Repositories\DocumentFolderRepositoryInterface;
use TangleMedia\Laravel\Documents\Interfaces\Services\DocumentFolderServiceInterface;

class DocumentFolderService implements DocumentFolderServiceInterface
{
    protected $document_folder_repository;

    public function __construct(DocumentFolderRepositoryInterface $repository)
    {
        $this->document_folder_repository = $repository;
    }

    /**
     * Get results paginated
     * @return mixed
     */
    public function getPaginated()
    {
        return $this->document_folder_repository->paginate();
    }

    /**
     * Get one record
     * @param string $document_folder_id
     * @return mixed
     */
    public function getOne(string $document_folder_id)
    {
        return $this->document_folder_repository->findOne($document_folder_id);
    }

    /**
     * Store new record
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        return $this->document_folder_repository->create($data);
    }

    /**
     * Update an existing record
     * @param array $data
     * @param string $document_folder_id
     * @return mixed
     */
    public function update(array $data, string $document_folder_id)
    {
        return $this->document_folder_repository->update($data, $document_folder_id);
    }

    /**
     * Delete record
     * @param string $document_folder_id
     * @return mixed
     */
    public function delete(string $document_folder_id)
    {
        return $this->document_folder_repository->delete($document_folder_id);
    }
}
