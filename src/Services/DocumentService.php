<?php

namespace TangleMedia\Laravel\Documents\Services;

use TangleMedia\Laravel\Documents\Helpers\DocumentUploadFile;
use TangleMedia\Laravel\Documents\Http\Requests\DocumentStoreRequest;
use TangleMedia\Laravel\Documents\Http\Requests\DocumentUpdateRequest;
use TangleMedia\Laravel\Documents\Interfaces\Repositories\DocumentRepositoryInterface;
use TangleMedia\Laravel\Documents\Interfaces\Services\DocumentServiceInterface;

class DocumentService implements DocumentServiceInterface
{
    protected $document_repository;

    public function __construct(DocumentRepositoryInterface $repository)
    {
        $this->document_repository = $repository;
    }

    /**
     * Get results paginated
     * @return mixed
     */
    public function getPaginated()
    {
        return $this->document_repository->paginate();
    }

    /**
     * Get one record
     * @param string $document_id
     * @return mixed
     */
    public function getOne(string $document_id)
    {
        return $this->document_repository->findOne($document_id);
    }

    /**
     * Store new record
     * @param DocumentStoreRequest $request
     * @param DocumentUploadFile $file
     * @return mixed
     */
    public function store(DocumentStoreRequest $request, DocumentUploadFile $file)
    {
        return $this->document_repository->create($file);
    }

    /**
     * Update an existing record
     * @param DocumentUpdateRequest $request
     * @param DocumentUploadFile $file
     * @param string $document_id
     * @return mixed
     */
    public function update(DocumentUpdateRequest $request, DocumentUploadFile $file, string $document_id)
    {
        return $this->document_repository->update($file, $document_id);
    }

    /**
     * Delete record
     * @param string $document_id
     * @return mixed
     */
    public function delete(string $document_id)
    {
        return $this->document_repository->delete($document_id);
    }
}
