<?php

namespace TangleMedia\Laravel\Documents\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use TangleMedia\Laravel\Documents\Interfaces\Models\DocumentInterface;
use TangleMedia\Laravel\Documents\Filters\DocumentFilter;

class Document extends Model implements DocumentInterface
{

    use SoftDeletes;

    protected $guarded = [];
    protected $appends = ['public_url'];

    public function __construct(array $attributes = [])
    {
        //$attributes['guard_name'] = $attributes['guard_name'] ?? config('auth.defaults.guard');
        parent::__construct($attributes);
        $this->setTable(config('documents.table_names.documents'));
    }

    /**
     * @param Builder $builder
     * @param $request
     * @return mixed
     */
    public function scopeFilter(Builder $builder, $request)
    {
        return (new DocumentFilter($request))->filter($builder);
    }

    public function getPublicUrlAttribute()
    {
        return $this->base_path . '/' . $this->relative_path . '/' . $this->filename;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function documentFolder(): BelongsTo
    {
        return $this->belongsTo(
            config('documents.models.document_folder'),
            'document_folder_id',
            'id'
        );
    }

}
