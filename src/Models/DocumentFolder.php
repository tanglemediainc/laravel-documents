<?php

namespace TangleMedia\Laravel\Documents\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use TangleMedia\Laravel\Documents\Filters\DocumentFolderFilter;
use TangleMedia\Laravel\Documents\Interfaces\Models\DocumentFolderInterface;

class DocumentFolder extends Model implements DocumentFolderInterface
{

    use SoftDeletes;

    protected $guarded = [];

    public function __construct(array $attributes = [])
    {
        //$attributes['guard_name'] = $attributes['guard_name'] ?? config('auth.defaults.guard');
        parent::__construct($attributes);
        $this->setTable(config('documents.table_names.document_folders'));
    }

    /**
     * @param Builder $builder
     * @param $request
     * @return mixed
     */
    public function scopeFilter(Builder $builder, $request)
    {
        return (new DocumentFolderFilter($request))->filter($builder);
    }

    /**
     * @return HasMany
     */
    public function documents(): HasMany
    {
        return $this->hasMany(
            config('documents.models.document'),
            'document_folder_id',
            'id'
        );
    }

    /**
     * @return BelongsTo
     */
    public function parentFolder(): BelongsTo
    {
        return $this->belongsTo(
            config('documents.models.document_folder'),
            'parent_folder_id',
            'id'
        );
    }

    /**
     * @return HasMany
     */
    public function childrenFolders(): HasMany
    {
        return $this->hasMany(
            config('documents.models.document_folder'),
            'parent_folder_id',
            'id'
        );
    }

}
