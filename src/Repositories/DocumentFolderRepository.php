<?php

namespace TangleMedia\Laravel\Documents\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use TangleMedia\Laravel\Documents\Interfaces\Models\DocumentFolderInterface;
use TangleMedia\Laravel\Documents\Interfaces\Repositories\DocumentFolderRepositoryInterface;

class DocumentFolderRepository implements DocumentFolderRepositoryInterface
{

    protected $document_folder;
    protected $request;
    protected $perPage;

    public function __construct(DocumentFolderInterface $document_folder, Request $request)
    {
        $this->document_folder = $document_folder;
        $this->request = $request;
        $this->perPage = config('documents.pagination.per_page');
    }

    /**
     * Document Folder model - Get results paginated
     * @return mixed
     */
    public function paginate()
    {
        return $this->document_folder::filter($this->request)->paginate($this->perPage);
    }

    /**
     * Document Folder model - Get single record
     * @param string $document_folder_id
     * @return mixed
     */
    public function findOne(string $document_folder_id)
    {
        $document_folder = $this->document_folder->where('id', $document_folder_id)->first();
        if (! $document_folder) throw new ModelNotFoundException("document_folder_not_found");
        return $document_folder;
    }

    /**
     * Document Folder model - Create record
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->document_folder::create($data);
    }

    /**
     * Document Folder model - Update record
     * @param array $data
     * @param string $document_folder_id
     * @return mixed
     */
    public function update(array $data, string $document_folder_id)
    {
        $document_folder = $this->findOne($document_folder_id);
        $document_folder->update($data);
        return $document_folder->fresh();
    }

    /**
     * Document Folder model - Delete record
     * @param string $document_folder_id
     * @return mixed
     */
    public function delete(string $document_folder_id)
    {
        $document_folder = $this->findOne($document_folder_id);
        return $document_folder->delete();
    }
}
