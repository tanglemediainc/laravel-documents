<?php

namespace TangleMedia\Laravel\Documents\Repositories;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use TangleMedia\Laravel\Documents\Exceptions\FileHelperException;
use TangleMedia\Laravel\Documents\Exceptions\FileHelperNotAllowedTypeException;
use TangleMedia\Laravel\Documents\Helpers\DocumentUploadFile;
use TangleMedia\Laravel\Documents\Helpers\FileHelper;
use TangleMedia\Laravel\Documents\Interfaces\Models\DocumentInterface;
use TangleMedia\Laravel\Documents\Interfaces\Repositories\DocumentRepositoryInterface;

class DocumentRepository implements DocumentRepositoryInterface
{

    protected $document;
    protected $request;
    protected $fileHelper;
    protected $perPage;

    public function __construct(DocumentInterface $document, Request $request, FileHelper $helper)
    {
        $this->document = $document;
        $this->request = $request;
        $this->fileHelper = $helper;
        $this->perPage = config('documents.pagination.per_page');
    }

    /**
     * Document model - Get results paginated
     * @return mixed
     */
    public function paginate()
    {
        return $this->document::filter($this->request)->paginate($this->perPage);
    }

    /**
     * Document model - Get single record
     * @param string $document_id
     * @return mixed
     */
    public function findOne(string $document_id)
    {
        $document = $this->document->where('id', $document_id)->first();
        if (! $document) throw new ModelNotFoundException("document_not_found");
        return $document;
    }

    /**
     * Document model - Create record
     * @param DocumentUploadFile $file
     * @return mixed
     * @throws Exception
     */
    public function create(DocumentUploadFile $file)
    {
        $documentData = $this->fileHelper->getFileInfo($file->getFile());

        return $this->document::create($file->without('file')->toArray() + $documentData);
    }

    /**
     * Document model - Update record
     * @param DocumentUploadFile $file
     * @param string $document_id
     * @return mixed
     * @throws FileHelperException
     * @throws FileHelperNotAllowedTypeException
     */
    public function update(DocumentUploadFile $file, string $document_id)
    {
        $document = $this->findOne($document_id);

        $documentData = ($file->getFile() ? $this->fileHelper->getFileInfo($file->getFile()) : []);

        $document->update($file->without('file')->toArray() + $documentData);

        return $document->fresh();
    }

//    public function update(DocumentUploadFile $file, string $document_id)
//    {
//        $document = $this->findOne($document_id);
//
//        $payload = ($file->without('file')->toArray() ?: []);
//        if($file->getFile()) {
//            $payload = $payload + $this->fileHelper->getFileInfo($file->getFile());
//        }
//
//        $document->update($payload);
//
//        return $document->fresh();
//    }

    /**
     * Document model - Delete record
     * @param string $document_id
     * @return mixed
     */
    public function delete(string $document_id)
    {
        return $this->findOne($document_id)->delete();
    }
}
