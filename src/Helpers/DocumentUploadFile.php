<?php


namespace TangleMedia\Laravel\Documents\Helpers;

use Illuminate\Http\UploadedFile;

class DocumentUploadFile
{
    protected $data = [];

    public function __construct(UploadedFile $file = null, $document_folder_id = null, string $name = null, string $description = null)
    {
        $this->data['file'] = $file;
        $this->data['document_folder_id'] = $document_folder_id;
        $this->data['name'] = $name;
        $this->data['description'] = $description;
    }

    public function getFile()
    {
        return $this->data["file"];
    }

    public function getDocumentFolderId()
    {
        return $this->data["document_folder_id"];
    }

    public function getName()
    {
        return $this->data["name"];
    }

    public function getDescription()
    {
        return $this->data["description"];
    }

    public function toArray()
    {
        return $this->data;
    }

    public function without($key)
    {
        unset($this->data[$key]);
        return $this;
    }

}
