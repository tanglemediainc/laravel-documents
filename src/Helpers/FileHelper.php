<?php

namespace TangleMedia\Laravel\Documents\Helpers;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use TangleMedia\Laravel\Documents\Exceptions\FileHelperException;

class FileHelper
{

    /**
     * @var UploadedFile $uploaded_file
     */
    protected $uploaded_file;
    protected $storage_path;

    protected $shard;
    protected $base_path;
    protected $hash;
    protected $ext;
    protected $mime_type;
    protected $file_name;
    protected $relative_path;
    protected $size;

    protected $data = [];

    public function getFileInfo(UploadedFile $file)
    {
        $this->storage_path = config('documents.storage_path', false);
        if(!$this->storage_path) {
            throw new FileHelperException("Provide a base file storage path.");
        }
        $this->uploaded_file = $file;
        $this->setData();
        $this->uploadFile();
        return $this->data;
    }

    /**
     * Set file data
     */
    protected function setData()
    {
        $this->setShard();
        $this->setBasePath();
        $this->setHash();
        $this->setExtension();
        $this->setMimeType();
        $this->setFilename();
        $this->setRelativePath();
        $this->setFileSize();
    }

    protected function setShard(): void
    {
        $dir1 = Str::random(1);
        $dir2 = Str::random(1);
        $dir3 = Str::random(1);
        $shard = $dir1 . '/' . $dir2 . '/' . $dir3;
        $shard = strtolower($shard);
        $this->shard = $shard;
        $this->data["shard"] = $this->shard;
    }

    protected function setBasePath(): void
    {
        $this->base_path = implode('/', array_filter([$this->storage_path, $this->shard]));
        $this->data["base_path"] = $this->base_path;
    }

    protected function setHash(): void
    {
        $this->hash = Str::uuid();
        $this->data["hash"] = $this->hash;
    }

    protected function setExtension(): void
    {
        $this->ext = $this->uploaded_file->getClientOriginalExtension();
        $this->data["ext"] = $this->ext;
    }

    protected function setMimeType(): void
    {
        $this->mime_type = $this->uploaded_file->getMimeType();
        $this->data["mime_type"] = $this->mime_type;
    }

    protected function setFileName(): void
    {
        $this->file_name = implode('.', [$this->hash, $this->ext]);
        $this->data["file_name"] = $this->file_name;
    }

    protected function setRelativePath(): void
    {
        $this->relative_path = implode('/', [$this->base_path, $this->file_name]);
        $this->data["relative_path"] = $this->relative_path;
    }

    protected function setFileSize(): void
    {
        $this->size = $this->uploaded_file->getSize();
        $this->data["size"] = $this->size;
    }

    /**
     * Upload file
     */
    protected function uploadFile(): void
    {
        $this->uploaded_file->storeAs($this->base_path, $this->file_name);
    }

}
