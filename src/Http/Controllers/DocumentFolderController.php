<?php

namespace TangleMedia\Laravel\Documents\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use TangleMedia\Laravel\Documents\Http\Requests\DocumentFolderStoreRequest;
use TangleMedia\Laravel\Documents\Http\Requests\DocumentFolderUpdateRequest;
use TangleMedia\Laravel\Documents\Http\Resources\DocumentFolderResource;
use TangleMedia\Laravel\Documents\Http\Resources\PaginationResource;
use TangleMedia\Laravel\Documents\Interfaces\Services\DocumentFolderServiceInterface;
use TangleMedia\Laravel\Documents\Http\Traits\JResponse;

class DocumentFolderController extends Controller
{
    use JResponse;

    protected $document_folder_service;

    /**
     * DocumentFolderController constructor.
     * @param DocumentFolderServiceInterface $service
     */
    public function __construct(DocumentFolderServiceInterface $service)
    {
        $this->document_folder_service = $service;
    }

    /**
     * Get documents paginated
     * @return JsonResponse
     */
    public function index()
    {
        $documents = $this->document_folder_service->getPaginated();

        $r = $this->successResponse('document_folder_list', [
            'document_folders' => DocumentFolderResource::collection($documents),
            'pagination' => new PaginationResource($documents)
        ]);

        return $r;
    }

    /**
     * Get single document
     * @param string $document_folder_id
     * @return JsonResponse
     */
    public function show(string $document_folder_id)
    {
        $document = $this->document_folder_service->getOne($document_folder_id);

        return $this->successResponse('show_document_folder', [
            'document_folder' => new DocumentFolderResource($document),
            'pagination' => null
        ]);
    }

    /**
     * Create new document
     * @param DocumentFolderStoreRequest $request
     * @return JsonResponse
     */
    public function store(DocumentFolderStoreRequest $request)
    {
        $document = $this->document_folder_service->store($request->validated());

        return $this->successResponse('document_folder_created', [
            'document_folder' => new DocumentFolderResource($document),
            'pagination' => null
        ]);
    }

    /**
     * Update document
     * @param DocumentFolderUpdateRequest $request
     * @param string $document_folder_id
     * @return JsonResponse
     */
    public function update(DocumentFolderUpdateRequest $request, string $document_folder_id)
    {
        $document = $this->document_folder_service->update($request->validated(), $document_folder_id);

        return $this->successResponse('document_folder_updated', [
            'document_folder' => new DocumentFolderResource($document),
            'pagination' => null
        ]);
    }

    /**
     * Delete document
     * @param string $document_folder_id
     * @return JsonResponse
     */
    public function delete(string $document_folder_id)
    {
        $this->document_folder_service->delete($document_folder_id);

        return $this->successResponse('document_folder_deleted');
    }
}
