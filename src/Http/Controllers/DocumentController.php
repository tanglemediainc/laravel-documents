<?php

namespace TangleMedia\Laravel\Documents\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use TangleMedia\Laravel\Documents\Helpers\DocumentUploadFile;
use TangleMedia\Laravel\Documents\Http\Requests\DocumentStoreRequest;
use TangleMedia\Laravel\Documents\Http\Requests\DocumentUpdateRequest;
use TangleMedia\Laravel\Documents\Http\Resources\PaginationResource;
use TangleMedia\Laravel\Documents\Http\Traits\JResponse;
use TangleMedia\Laravel\Documents\Interfaces\Services\DocumentServiceInterface;

class DocumentController extends Controller
{
    use JResponse;

    protected $document_service;

    /**
     * DocumentController constructor.
     * @param DocumentServiceInterface $service
     */
    public function __construct(DocumentServiceInterface $service)
    {
        $this->document_service = $service;
        $this->document_resource = config('documents.resources.document');
    }

    /**
     * Get documents paginated
     * @return JsonResponse
     */
    public function index()
    {
        $documents = $this->document_service->getPaginated();

        return $this->successResponse('document_list', [
            'documents' => $this->document_resource::collection($documents),
            'pagination' => new PaginationResource($documents)
        ]);
    }

    /**
     * Get single document
     * @param string $document_id
     * @return JsonResponse
     */
    public function show(string $document_id)
    {
        $document = $this->document_service->getOne($document_id);

        return $this->successResponse('show_document', [
            'document' => $this->document_resource::make($document),//new DocumentResource($document),
            'pagination' => null
        ]);
    }

    /**
     * Create new document
     * @param DocumentStoreRequest $request
     * @param DocumentUploadFile $file
     * @return JsonResponse
     */
    public function store(DocumentStoreRequest $request, DocumentUploadFile $file)
    {
        $document = $this->document_service->store($request, $file);

        return $this->successResponse('document_created', [
            'document' => $this->document_resource::make($document),
            'pagination' => null
        ]);
    }

    /**
     * Update document
     * @param DocumentUpdateRequest $request
     * @param DocumentUploadFile $file
     * @param string $document_id
     * @return JsonResponse
     */
    public function update(DocumentUpdateRequest $request, DocumentUploadFile $file, string $document_id)
    {
        $document = $this->document_service->update($request, $file, $document_id);

        return $this->successResponse('document_updated', [
            'document' => $this->document_resource::make($document),
            'pagination' => null
        ]);
    }

    /**
     * Delete document
     * @param string $document_id
     * @return JsonResponse
     */
    public function delete(string $document_id)
    {
        $this->document_service->delete($document_id);

        return $this->successResponse('document_deleted');
    }

    public function download(string $document_id) {
        $document = $this->document_service->getOne($document_id);
        return response()->download(storage_path('app').'/'.$document->relative_path);
    }

    public function file(string $document_id) {
        $document = $this->document_service->getOne($document_id);
        return response()->file(storage_path('app').'/'.$document->relative_path);
    }

}
