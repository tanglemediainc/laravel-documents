<?php

namespace TangleMedia\Laravel\Documents\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocumentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        //dd(request()->file('file')->getMimeType());
        return [
            'name' => 'nullable|string',
            'description' => 'nullable|string',
            'file' => [
                'required',
                'max:'.config('documents.upload.max_size'),
                'mimetypes:'.config('documents.upload.mimes')
            ]
        ];
    }
}
