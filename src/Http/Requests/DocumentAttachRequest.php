<?php

namespace TangleMedia\Laravel\Documents\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocumentAttachRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'document_id' => 'required|integer',
            'model_type' => 'required|string',
            'model_id' => 'required|integer',
            'ref' => 'nullable|string'
        ];
    }
}
