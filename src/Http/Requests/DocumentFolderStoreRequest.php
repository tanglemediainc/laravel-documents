<?php

namespace TangleMedia\Laravel\Documents\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocumentFolderStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'parent_folder_id' => 'nullable|integer',
            'private' => 'nullable|boolean',
            'name' => 'nullable|string',
            'icon' => 'nullable|string',
        ];
    }
}
