<?php

namespace TangleMedia\Laravel\Documents\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use TangleMedia\Laravel\Documents\Interfaces\Resources\DocumentResourceInterface;

class DocumentResource extends JsonResource implements DocumentResourceInterface
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'document_folder' => new DocumentFolderResource($this->documentFolder),
            'name' => $this->name,
            'ext' => $this->ext,
            'mime_type' => $this->mime_type,
            //'shard' => $this->shard,
            //'hash' => $this->hash,
            //'file_name' => $this->file_name,
            //'base_path' => $this->base_path,
            //'relative_path' => $this->relative_path,
            'size' => $this->size,
            'description' => $this->description,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'download_url' => url("/documents/{$this->id}/download"),
            'file_url' => url("/documents/{$this->id}/file")
        ];
    }
}
