<?php

namespace TangleMedia\Laravel\Documents\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use TangleMedia\Laravel\Documents\Interfaces\Resources\DocumentFolderResourceInterface;

class DocumentFolderResource extends JsonResource implements DocumentFolderResourceInterface
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'parent_folder' => new DocumentFolderResource($this->parentFolder),
            'name' => $this->name,
            'icon' => $this->icon,
            'private' => $this->private,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
