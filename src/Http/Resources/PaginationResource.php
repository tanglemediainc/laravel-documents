<?php

namespace TangleMedia\Laravel\Documents\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PaginationResource extends JsonResource
{
    protected $filters = [];
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        preg_match("/&?page=([^&]+)/", $this->previousPageUrl(), $match_prv);
        preg_match("/&?page=([^&]+)/", $this->nextPageUrl(), $match_nxt);
        $params = http_build_query($request->except('page'),null,'&');

        return [
            'total' => $this->total(),
            'displayed' => $this->count(),
            'previous' => isset($match_prv[1]) ? (int) $match_prv[1] : null,
            'current' => $this->currentPage(),
            'next' => isset($match_nxt[1]) ? (int) $match_nxt[1] : null,
            'next_page_url' => ! empty($params) ? $this->getNextPageUrl($params) : $this->nextPageUrl(),
            'previous_page_url' => ! empty($params) ? $this->getPreviousPageUrl($params) : $this->previousPageUrl()
        ];
    }

    /**
     * @param string $params
     * @return string
     */
    protected function getNextPageUrl(string $params)
    {
        $nextPageUrl = $this->nextPageUrl();
        return ($nextPageUrl != "") ? $nextPageUrl . '&' . $params : null;
    }

    /**
     * @param string $params
     * @return string
     */
    protected function getPreviousPageUrl(string $params)
    {
        $previousPageUrl = $this->previousPageUrl();
        return ($previousPageUrl != "") ? $previousPageUrl . '&' . $params : null;
    }
}
