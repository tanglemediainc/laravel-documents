<?php

namespace TangleMedia\Laravel\Documents\Http\Traits;

trait JResponse
{

    public function successResponse(string $message, array $data = [])
    {
        return response()->json([
            'message' => $message,
            'data' => $data,
            'success' => true
        ], 200);
    }

    public function errorResponse(string $message, array $data = [])
    {
        return response()->json([
            'message' => $message,
            'data' => $data,
            'success' => false
        ], 400);
    }

}
