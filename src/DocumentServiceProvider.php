<?php

namespace TangleMedia\Laravel\Documents;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use TangleMedia\Laravel\Documents\Helpers\DocumentUploadFile;
use TangleMedia\Laravel\Documents\Interfaces\Models\DocumentFolderInterface;
use TangleMedia\Laravel\Documents\Interfaces\Models\DocumentInterface;
use TangleMedia\Laravel\Documents\Interfaces\Repositories\DocumentFolderRepositoryInterface;
use TangleMedia\Laravel\Documents\Interfaces\Repositories\DocumentRepositoryInterface;
use TangleMedia\Laravel\Documents\Interfaces\Services\DocumentFolderServiceInterface;
use TangleMedia\Laravel\Documents\Interfaces\Services\DocumentServiceInterface;

class DocumentServiceProvider extends ServiceProvider
{

    public function boot(Filesystem $filesystem)
    {
        if (function_exists('config_path')) {
            $this->publishes([
                __DIR__ . '/../config/documents.php' => config_path('documents.php'),
            ], 'config');
            $this->publishes([
                __DIR__ . '/../database/migrations/create_document_tables.php.stub' => $this->getMigrationFileName($filesystem),
            ], 'migrations');
        }

        $this->commands([]);

        $this->registerModelBindings();
        //$this->registerResourceBindings();
        $this->registerServiceBindings();
        $this->registerRepositoryBindings();
        $this->registerOtherBindings();
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/documents.php',
            'documents'
        );
    }

    protected function registerModelBindings()
    {
        $config = $this->app->config['documents.models'];
        if(!$config) {
            return;
        }

        $this->app->bind(DocumentInterface::class, $config['document']);
        $this->app->bind(DocumentFolderInterface::class, $config['document_folder']);
    }

    // protected function registerResourceBindings()
    // {
    //     $config = $this->app->config['documents.resources'];
    //     if(!$config) {
    //         return;
    //     }

    //     $this->app->bind(DocumentResourceInterface::class, $config['document']);
    //     $this->app->bind(DocumentResourceFolderInterface::class, $config['document_folder']);
    // }

    protected function registerServiceBindings()
    {
        $config = $this->app->config['documents.services'];
        if(!$config) {
            return;
        }

        $this->app->bind(DocumentServiceInterface::class, $config['document']);
        $this->app->bind(DocumentFolderServiceInterface::class, $config['document_folder']);
    }

    protected function registerRepositoryBindings()
    {
        $config = $this->app->config['documents.repositories'];
        if(!$config) {
            return;
        }

        $this->app->bind(DocumentRepositoryInterface::class, $config['document']);
        $this->app->bind(DocumentFolderRepositoryInterface::class, $config['document_folder']);
    }

    protected function registerOtherBindings()
    {
        $this->app->bind(DocumentUploadFile::class, function() {
            $request = app(Request::class);

            $file = $request->file('file');
            $folder_id = $request->get('document_folder_id') ?: $request->json('document_folder_id');
            $name = $request->get('name') ?: $request->json('name');
            $description = $request->get('description') ?: $request->json('description');

            $name = ($name ?: ($file ? $file->getClientOriginalName() : null));

            return new DocumentUploadFile($file, $folder_id, $name, $description);
        });

    }

    /**
     * Returns existing migration file if found, else uses the current timestamp.
     *
     * @param Filesystem $filesystem
     * @return string
     */
    protected function getMigrationFileName(Filesystem $filesystem): string
    {
        $timestamp = date('Y_m_d_His');

        return Collection::make($this->app->databasePath().DIRECTORY_SEPARATOR.'migrations'.DIRECTORY_SEPARATOR)
            ->flatMap(function ($path) use ($filesystem) {
                return $filesystem->glob($path.'*_create_document_tables.php');
            })->push($this->app->databasePath()."/migrations/{$timestamp}_create_document_tables.php")
            ->first();
    }
}
