<?php

Route::group([
    'prefix' => 'documents'
],function () {
    $namespace = '\TangleMedia\Laravel\Documents\Http\Controllers\\';
	// Get documents paginated
	Route::get('/', $namespace.'DocumentController@index')->name('api.v1.documents.get');
	// Show documents
	Route::get('/{document_id}', $namespace.'DocumentController@show')->name('api.v1.documents.show');
	// Store document
	Route::post('/', $namespace.'DocumentController@store')->name('api.v1.documents.store');
	// Update document
	Route::put('/{document_id}', $namespace.'DocumentController@update')->name('api.v1.documents.update');
	// Delete document
	Route::delete('/{document_id}', $namespace.'DocumentController@delete')->name('api.v1.documents.delete');
    // View document
    Route::get('/inline/{document_id}', $namespace.'DocumentController@inline')->name('api.v1.documents.inline');
});
