<?php

Route::group([
    'prefix' => 'document_folders'
],function () {
    $namespace = '\TangleMedia\Laravel\Documents\Http\Controllers\\';
	// Get document folder folders paginated
	Route::get('/', $namespace.'DocumentFolderController@index')->name('api.v1.document_folders.get');
	// Show document folder folders
	Route::get('/{document_folder_id}', $namespace.'DocumentFolderController@show')->name('api.v1.document_folders.show');
	// Store document folder
	Route::post('/', $namespace.'DocumentFolderController@store')->name('api.v1.document_folders.store');
	// Update document folder
	Route::put('/{document_folder_id}', $namespace.'DocumentFolderController@update')->name('api.v1.document_folders.update');
    // Delete document folder
	Route::delete('/{document_folder_id}', $namespace.'DocumentFolderController@delete')->name('api.v1.document_folders.delete');
});
