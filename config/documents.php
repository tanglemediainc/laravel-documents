<?php

return [

    'models' => [
        'document' => TangleMedia\Laravel\Documents\Models\Document::class,
        'document_folder' => TangleMedia\Laravel\Documents\Models\DocumentFolder::class,
    ],

    'resources' => [
        'document' => TangleMedia\Laravel\Documents\Http\Resources\DocumentResource::class,
        'document_folder' => TangleMedia\Laravel\Documents\Http\Resources\DocumentFolderResource::class,
    ],

    'services' => [
        'document' => TangleMedia\Laravel\Documents\Services\DocumentService::class,
        'document_folder' => TangleMedia\Laravel\Documents\Services\DocumentFolderService::class,
    ],

    'repositories' => [
        'document' => TangleMedia\Laravel\Documents\Repositories\DocumentRepository::class,
        'document_folder' => TangleMedia\Laravel\Documents\Repositories\DocumentFolderRepository::class,
    ],

    'table_names' => [
        'documents' => 'documents',
        'document_folders' => 'document_folders',
    ],

    'root_path' => 'private',

    'uses_uuid' => false,
    'uses_encryption' => false,

    'upload' => [
        'max_size' => 2048,
        'mimes' => 'xlsx,doc,docx,ppt,pptx,ods,odt,odp,txt',
    ],

];
